import random

themes = {"Agriculture":["Batteuse","Beche","Moisson","Labour","Silo","Fermier"],"Anatomie":["Cheveu","Cil","Cloison"]}

print("selection des theme")
conteur = 0
for i in themes:
    print(conteur, " : " , i)
    conteur += 1

choix  = int(input(" votre choix"))
choixtext = list( themes.keys() )[choix]

text = random.choice(themes[choixtext])
text = text.upper()

mot_a_trouver = list( "_" * len(text) )   # _ _ _ _
compteur = 11

while True:
    print( " ".join(mot_a_trouver) )
    print('compteur :', compteur)
    lettre = input("votre lettre : ")

    val = 0
    temp = False
    for i in text:
        if i == lettre.upper():
            mot_a_trouver[val] = i
            temp = True
        val = val + 1

    if temp == False:
        compteur = compteur - 1

    if list(text) == mot_a_trouver:
        print(" ".join(mot_a_trouver))
        print(" cool ")
        break

    if compteur == 0:
        print(" hahahahahaha")
        break